# Read `man 5 sway` for a complete reference.

### Variables
#
# Logo key. Use Mod1 for Alt.
set $mod Mod4
# Home row direction keys, like vim
set $left h
set $down j
set $up k
set $right l
# Your preferred terminal emulator
set $term $HOME/.config/i3/terminal.sh
# Your preferred application launcher
# Note: it's recommended that you pass the final command to sway
#set $menu dmenu_path | dmenu | xargs swaymsg exec --
set $menu swaymsg -- exec "sh -c 'killall rofi || rofi -show combi'"
set $lock swaylock -c 000000 -l -f -i $HOME/.config/sway/background_lock

xwayland force

### Output configuration
#
# Default wallpaper (more resolutions are available in /usr/share/backgrounds/sway/)
output * bg $HOME/.config/sway/background fill
#
# Example configuration:
#
#   output HDMI-A-1 resolution 1920x1080 position 1920,0
#
# You can get the names of your outputs by running: swaymsg -t get_outputs

### Idle configuration
#
# Example configuration:
#
# exec swayidle -w \
#          timeout 300 'swaylock -f -c 000000' \
#          timeout 600 'swaymsg "output * dpms off"' \
#               resume 'swaymsg "output * dpms on"' \
#          before-sleep 'swaylock -f -c 000000'
#
# This will lock your screen after 300 seconds of inactivity, then turn off
# your displays after another 300 seconds, and turn your screens back on when
# resumed. It will also lock your screen before your computer goes to sleep.

### Input configuration
#
# Example configuration:
#
#   input "2:14:SynPS/2_Synaptics_TouchPad" {
#       dwt enabled
#       tap enabled
#       natural_scroll enabled
#       middle_emulation enabled
#   }
#
# You can get the names of your inputs by running: swaymsg -t get_inputs
# Read `man 5 sway-input` for more information about this section.
	#input "2:7:SynPS/2_Synaptics_TouchPad" {

	# Touchpad config
	input type:touchpad {
		accel_profile adaptive
		pointer_accel 0.5
		#dwt disabled
		tap enabled
		drag enabled
		tap_button_map lrm
		middle_emulation enabled
		events disabled_on_external_mouse

		#natural_scroll enabled
		#scroll_factor 0.1
		# There's a patch to the source code to allow two_finger,edge
		# /etc/portage/patches/dev-libs/libinput/always-enable-2fg-scroll.patch
	}

	input "5050:23:Generic_USB_K/B_Mouse" {
		events disabled
	}

	# Keyboard config
	input type:keyboard {
		xkb_layout "be,us"
		xkb_numlock enabled
		xkb_options "altwin:swap_lalt_lwin,compose:menu,shift:both_capslock,caps:none"
		repeat_delay 170
		repeat_rate 70
	}

	input "1452:545:Apple,_Inc_Apple_Keyboard" {
		xkb_layout "be,us"
		xkb_numlock enabled
		xkb_options "compose:menu,shift:both_capslock,caps:none"
		repeat_delay 170
		repeat_rate 70
	}

	input "9011:26214:ydotoold_virtual_device" {
		xkb_layout "us"
		xkb_numlock enabled
		xkb_options ""
		repeat_delay 170
		repeat_rate 70
	}

	# For barcode scanners
	input "5050:23:Generic_USB_K/B" {
		xkb_layout "us"
		repeat_delay 20000
		repeat_rate 1
	}

	seat * hide_cursor 3000

	bindsym $mod+f1 exec $HOME/.config/sway/toggle_touchpad

### Appearance
#client.focused           #ffffff33 #ffffff33 #ffffff #ffffff #ffffff22
#client.focused_inactive  #ffffff11 #ffffff11 #999999 #999999 #ffffff00
#client.unfocused         #ffffff00 #ffffff00 #999999 #999999 #ffffff00
#client.urgent            #ff000066 #ff000066 #ffffff #ffffff #ff000066

##                        Titl brdr  Titl fill                    Wndw brdr
##client.focused           #00000033  #00000033  #ffffff  #ffffff  #00000033
#client.focused           #ffffff33  #ffffff33  #ffffff  #ffffff  #ffffff33
#client.focused_inactive  #111111f2  #111111f2  #999999  #999999  #ffffff22
#client.unfocused         #00000088  #00000088  #999999  #999999  #00000088
#client.urgent            #ff000066  #ff000066  #ffffff  #ffffff  #ff000066

#                        Titl brdr  Titl fill                    Wndw brdr
client.focused           #222222d8  #222222d8  #ffffff  #ffffff  #ffffff33
client.focused_inactive  #111111d8  #111111d8  #999999  #999999  #111111d8
client.unfocused         #111111d8  #111111d8  #999999  #999999  #111111d8
client.urgent            #ff000066  #ff000066  #ffffff  #ffffff  #ff000066



font "Fira Code 7"

default_border none

titlebar_padding 3 2
#titlebar_border_thickness 0
smart_gaps on
smart_borders on
gaps {
	inner 6
}

tiling_drag disable

### Key bindings
#
# Basics:
#
	# start a terminal
	bindsym $mod+Return exec $term
	bindsym $mod+w exec $term

	# kill focused window
	bindsym $mod+Shift+q kill

	# start your launcher
	bindsym $mod+d exec $menu
	# unicode (i.a. emoji) picker
	bindsym $mod+Shift+d exec "killall rofi || rofimoji --skin-tone neutral --prompt 'unicode >'"

	# Drag floating windows by holding down $mod and left mouse button.
	# Resize them with right mouse button + $mod.
	# Despite the name, also works for non-floating windows.
	# Change normal to inverse to use left mouse button for resizing and right
	# mouse button for dragging.
	floating_modifier $mod normal

	# reload the configuration file
	bindsym $mod+Shift+c reload

	# exit sway (logs you out of your Wayland session)
	bindsym $mod+Shift+e exec swaynag -t warning -m 'You pressed the exit shortcut. Do you really want to exit sway? This will end your Wayland session.' -b 'Yes, exit sway' 'swaymsg exit'

	# lock screen
	bindsym $mod+o exec $lock
	bindswitch lid:on exec $lock
#
# Moving around:
#
	# Move your focus around
	bindsym $mod+$left focus left
	bindsym $mod+$down focus down
	bindsym $mod+$up focus up
	bindsym $mod+$right focus right
	# or use $mod+[up|down|left|right]
	bindsym $mod+Left focus left
	bindsym $mod+Down focus down
	bindsym $mod+Up focus up
	bindsym $mod+Right focus right

	# _move_ the focused window with the same, but add Shift
	bindsym $mod+Shift+$left move left
	bindsym $mod+Shift+$down move down
	bindsym $mod+Shift+$up move up
	bindsym $mod+Shift+$right move right
	# ditto, with arrow keys
	bindsym $mod+Shift+Left move left
	bindsym $mod+Shift+Down move down
	bindsym $mod+Shift+Up move up
	bindsym $mod+Shift+Right move right
#
# Workspaces:
#
	# switch to workspace
	bindsym $mod+ampersand  workspace number 1
	bindsym $mod+eacute     workspace number 2
	bindsym $mod+quotedbl   workspace number 3
	bindsym $mod+apostrophe workspace number 4
	bindsym $mod+parenleft  workspace number 5
	bindsym $mod+section    workspace number 6
	bindsym $mod+egrave     workspace number 7
	bindsym $mod+exclam     workspace number 8
	bindsym $mod+ccedilla   workspace number 9
	bindsym $mod+agrave     workspace number 10
	bindsym $mod+parenright workspace number 11
	# Rehab for switching around workspaces all the time
	#bindsym $mod+ampersand  exec "sleep 1; swaymsg 'workspace number 1'"
	#bindsym $mod+eacute     exec "sleep 1; swaymsg 'workspace number 2'"
	#bindsym $mod+quotedbl   exec "sleep 1; swaymsg 'workspace number 3'"
	#bindsym $mod+apostrophe exec "sleep 1; swaymsg 'workspace number 4'"
	#bindsym $mod+parenleft  exec "sleep 1; swaymsg 'workspace number 5'"
	#bindsym $mod+section    exec "sleep 1; swaymsg 'workspace number 6'"
	#bindsym $mod+egrave     exec "sleep 1; swaymsg 'workspace number 7'"
	#bindsym $mod+exclam     exec "sleep 1; swaymsg 'workspace number 8'"
	#bindsym $mod+ccedilla   exec "sleep 1; swaymsg 'workspace number 9'"
	#bindsym $mod+agrave     exec "sleep 1; swaymsg 'workspace number 10'"
	#bindsym $mod+parenright exec "sleep 1; swaymsg 'workspace number 11'"
	# move focused container to workspace
	bindsym $mod+1 move container to workspace number 1
	bindsym $mod+2 move container to workspace number 2
	bindsym $mod+3 move container to workspace number 3
	bindsym $mod+4 move container to workspace number 4
	bindsym $mod+5 move container to workspace number 5
	bindsym $mod+6 move container to workspace number 6
	bindsym $mod+7 move container to workspace number 7
	bindsym $mod+8 move container to workspace number 8
	bindsym $mod+9 move container to workspace number 9
	bindsym $mod+0 move container to workspace number 10
	bindsym $mod+degree move container to workspace number 11

bindsym $mod+S exec $HOME/.config/sway/rename.sh
bindsym $mod+shift+S exec $HOME/.config/sway/rename.sh --with-number

bindsym $mod+x move workspace to output right
bindsym $mod+shift+x move workspace to output up

#
# Layout stuff:
#
	# You can "split" the current object of your focus with
	# $mod+b or $mod+v, for horizontal and vertical splits
	# respectively.
	bindsym $mod+b splith
	bindsym $mod+v splitv

	# Switch the current container between different layout styles
	#bindsym $mod+s layout stacking
	bindsym $mod+z layout tabbed
	bindsym $mod+e layout toggle split

	# Make the current focus fullscreen
	bindsym $mod+f fullscreen

	# Toggle the current focus between tiling and floating mode
	bindsym $mod+Shift+space floating toggle

	# Swap focus between the tiling area and the floating area
	bindsym $mod+space focus mode_toggle

	# move focus to the parent container
	bindsym $mod+a focus parent
#
# Scratchpad:
#
	# Sway has a "scratchpad", which is a bag of holding for windows.
	# You can send windows there and get them back later.

	# Move the currently focused window to the scratchpad
	bindsym $mod+Shift+minus move scratchpad

	# Show the next scratchpad window or hide the focused scratchpad window.
	# If there are multiple scratchpad windows, this command cycles through them.
	bindsym $mod+minus scratchpad show

#
# Sticky:
#
	bindsym $mod+t sticky toggle
#
# Resizing containers:
#
mode "resize" {
	# small increments
	bindsym $left resize shrink width 50px
	bindsym $down resize grow  height 50px
	bindsym $up resize shrink  height 50px
	bindsym $right resize grow  width 50px
	bindsym Left resize shrink  width 50px
	bindsym Down resize grow   height 50px
	bindsym Up resize shrink   height 50px
	bindsym Right resize grow   width 50px

	# Shift: small increments
	bindsym Shift+$left resize shrink width 10px
	bindsym Shift+$down resize grow  height 10px
	bindsym Shift+$up resize shrink  height 10px
	bindsym Shift+$right resize grow  width 10px
	bindsym Shift+Left resize shrink  width 10px
	bindsym Shift+Down resize grow   height 10px
	bindsym Shift+Up resize shrink   height 10px
	bindsym Shift+Right resize grow   width 10px

	# Ctrl+Shift: huge increments
	bindsym Ctrl+Shift+$left resize shrink width 200px
	bindsym Ctrl+Shift+$down resize grow  height 200px
	bindsym Ctrl+Shift+$up resize shrink  height 200px
	bindsym Ctrl+Shift+$right resize grow  width 200px
	bindsym Ctrl+Shift+Left resize shrink  width 200px
	bindsym Ctrl+Shift+Down resize grow   height 200px
	bindsym Ctrl+Shift+Up resize shrink   height 200px
	bindsym Ctrl+Shift+Right resize grow   width 200px

	# return to default mode
	bindsym Return mode "default"
	bindsym Escape mode "default"
	bindsym $mod+r mode "default"
}
bindsym $mod+r mode "resize"

#
# MPD:
#
bindsym --locked XF86AudioPlay exec "mpc toggle"
bindsym --locked XF86AudioPrev exec "mpc cdprev"
bindsym --locked XF86AudioNext exec "mpc next"
bindsym --locked XF86AudioLowerVolume nop
bindsym --locked XF86AudioRaiseVolume nop
bindsym --locked XF86AudioMute nop

set $mpc /usr/bin/mpc
set $mpdmode "local mpd: [ ] play/pause, [k/j] prev/next, [h/l] rwnd/ffwd, [u/d] vol, [enter] open client"
set $mpdclient $term -e ncmpcpp
mode $mpdmode {
	bindsym space     exec --no-startup-id $mpc toggle, mode default
	bindsym j         exec --no-startup-id $mpc next
	bindsym k         exec --no-startup-id $mpc cdprev
	bindsym h         exec --no-startup-id $mpc seek -5
	bindsym l         exec --no-startup-id $mpc seek +5
	bindsym Backspace exec --no-startup-id $mpc seek 0%
	bindsym u         exec --no-startup-id $mpc volume +$volumeStep
	bindsym d         exec --no-startup-id $mpc volume -$volumeStep
	bindsym s         exec --no-startup-id $mpc stop, mode "default"
	bindsym Return    exec $mpdclient, mode "default"
	bindsym KP_Enter  exec $mpdclient, mode "default"
	bindsym o         exec $mpdclient, mode "default"

	# back to normal: Escape or mod+m
	bindsym Escape   mode "default"
	bindsym $mod+m   mode "default"
}
bindsym $mod+m mode $mpdmode

# ---

bindsym Print exec grim -g "$(slurp)" "/tmp/screenshot-$(date +%Y-%m-%d_%H-%M-%S).png"

for_window [window_role = "pop-up"] floating enable
for_window [window_role = "bubble"] floating enable
for_window [window_role = "dialog"] floating enable
for_window [window_type = "dialog"] floating enable
for_window [window_role = "task_dialog"] floating enable
for_window [window_type = "menu"] floating enable
for_window [app_id = "floating"] floating enable
for_window [app_id = "floating_update"] floating enable, resize set width 1000px height 600px
for_window [class = "(?i)pinentry"] floating enable
for_window [title = "Administrator privileges required"] floating enable

for_window [title="^Firefox — Sharing Indicator$"] {
	floating enable
	resize set 75 29
	move position 500 -20
}
for_window [title="^$" app_id="^firefox$"] {
	floating enable
	sticky enable
	move position 100 -20
}
for_window [title="^Developer Tools" app_id="^firefox$"] {
	floating disable
}
for_window [class="Tor Browser"] floating enable
for_window [class="org-openstreetmap-josm-gui-MainApplication" window_type=normal] {
	floating disable
	fullscreen disable
}
for_window [class="org-openstreetmap-josm-gui-MainApplication" window_type=dialog] {
	floating enable
	fullscreen disable
}
for_window [class="org-openstreetmap-josm-gui-MainApplication" title="^ $"] {
	floating enable
	fullscreen disable
}
for_window [class="org-openstreetmap-josm-gui-MainApplication" title="^Download$|^Search$"] {
	floating disable
	fullscreen disable
}
for_window [class="QjackCtl" title="^JACK Audio Connection Kit"] {
	floating enable
	move to workspace number 11
}
for_window [class="XEyes"] {
	floating enable
}

bindsym --locked --no-repeat Mod1+p exec $HOME/.config/sway/edp
bindsym $mod+n [title="^$" app_id="^firefox$"] kill

bindsym $mod+p exec $HOME/.local/bin/dpass

exec $HOME/.startup

#
# Status Bar:
#
# Read `man 5 sway-bar` for more information about this section.
#bar {
	#position top
	#font "Fira Code 10"
	##font "xos4 Terminus 9"

	## When the status_command prints a new line to stdout, swaybar updates.
	## The default just shows the current date and time.
	#status_command "$HOME/.local/bin/bar 2>&1"

	#colors {
		#background #111111d8
		#statusline #fffffff2
		#focused_workspace #111111d8 #111111d8 #ffffffff
		#active_workspace #000000ff #000000ff #aaaaffff
		#urgent_workspace #a33040 #a33040 #ffffff
		#inactive_workspace #111111d8 #111111d8 #777777ff
	#}
#}
exec yambar

exec xrdb -merge "$HOME/.Xresources"

exec DOTOOL_XKB_LAYOUT=be dotoold
exec wl-paste --watch clipman store
bindsym $mod+c exec "clipman pick --tool rofi"

include /etc/sway/config.d/*
