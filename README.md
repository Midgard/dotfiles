# Midgard's dotfiles

## Screenshots

![Desktop showing nvim, screenfetch and ncmpcpp](.repo-stuff/desktop.png)
![Desktop showing a single terminal, seamlessly blending with the bar](.repo-stuff/single-terminal.png)
![Open Rofi launcher](.repo-stuff/rofi.png)

When a CPU core is used, a graph for it appears. When the system temperature gets high, the temperature and fan speed are displayed. When RAM is running out, a warning is shown (not pictured).

![Bar showing CPU and temperature indicators](.repo-stuff/bar-load.png)

## Requirements

The Arch Linux / Gentoo Linux packages to install are between parentheses.

Note that inclusion of a package here does not imply that I claim that it is safe. Exercise the
usual due **caution when dealing with [AUR](https://aur.archlinux.org/) or
[overlay](https://overlays.gentoo.org/) packages.**

If you choose X:
* i3
  ([community/i3-wm](https://www.archlinux.org/packages/community/x86_64/i3-wm/) /
  [x11-wm/i3-gaps](https://packages.gentoo.org/packages/x11-wm/i3-gaps),
  window manager)
* Polybar
  ([community/polybar](https://archlinux.org/packages/community/x86_64/polybar/) /
  [x11-misc/polybar](https://packages.gentoo.org/packages/x11-misc/polybar),
  system bar)
* Dunst
  ([community/dunst](https://www.archlinux.org/packages/community/x86_64/dunst/) /
  [x11-misc/dunst](https://packages.gentoo.org/packages/x11-misc/dunst),
  notification daemon)
* python-notify2
  ([extra/python-notify2](https://archlinux.org/packages/extra/any/python-notify2/) /
  [dev-python/notify2](https://packages.gentoo.org/packages/dev-python/notify2),
  library to send notifications)
* xrandr and arandr
  ([extra/xorg-xrandr](https://www.archlinux.org/packages/extra/x86_64/xorg-xrandr/) and
  [community/arandr](https://www.archlinux.org/packages/community/any/arandr/) /
  [x11-apps/xrandr](https://packages.gentoo.org/packages/x11-apps/xrandr) and
  [x11-misc/arandr](https://packages.gentoo.org/packages/x11-misc/arandr))
* unclutter
  ([community/unclutter](https://www.archlinux.org/packages/community/x86_64/unclutter/) /
  [x11-misc/unclutter](https://packages.gentoo.org/packages/x11-misc/unclutter),
  hide the mouse when it hasn't moved for some time)
* xfce4-terminal
  ([extra/xfce4-terminal](https://archlinux.org/packages/extra/x86_64/xfce4-terminal/) /
  [x11-terms/xfce4-terminal](https://packages.gentoo.org/packages/x11-terms/xfce4-terminal),
  but you can choose your own terminal of course if you want)

If you choose Wayland:
* sway
  ([community/sway](https://archlinux.org/packages/community/x86_64/sway/) /
  [gui-wm/sway](https://packages.gentoo.org/packages/gui-wm/sway),
  Wayland compositor + window manager)
* yambar
  ([aur/yambar](https://aur.archlinux.org/packages/yambar) /
  [gui-apps/yambar::guru](https://gitweb.gentoo.org/repo/proj/guru.git/tree/gui-apps/yambar),
  system bar)
* foot
  ([community/foot](https://archlinux.org/packages/community/x86_64/foot/) /
  [gui-apps/foot](https://packages.gentoo.org/packages/gui-apps/foot),
  but you can choose your own terminal of course if you want)

Both X and Wayland:
* Rofi
  ([community/rofi](https://www.archlinux.org/packages/community/x86_64/rofi/),
  [x11-misc/rofi](https://packages.gentoo.org/packages/x11-misc/rofi)
  launcher)
* Mozilla's Fira fonts
  ([community/otf-fira-code](https://www.archlinux.org/packages/community/x86_64/otf-fira-code/),
  [community/otf-fira-mono](https://www.archlinux.org/packages/community/x86_64/otf-fira-mono/) and
  [community/otf-fira-sans](https://www.archlinux.org/packages/community/x86_64/otf-fira-sans/) /
  [media-fonts/fira-code](https://packages.gentoo.org/packages/media-fonts/fira-code),
  [media-fonts/fira-mono](https://packages.gentoo.org/packages/media-fonts/fira-mono) and
  [media-fonts/fira-sans](https://packages.gentoo.org/packages/media-fonts/fira-sans))
* Source Code Pro font
  ([aur/ttf-adobe-source-code-pro-fonts](https://aur.archlinux.org/packages/ttf-adobe-source-code-pro-fonts) /
  [media-fonts/source-code-pro](https://packages.gentoo.org/packages/media-fonts/source-code-pro))
* NeoVim
  ([community/neovim](https://www.archlinux.org/packages/community/x86_64/neovim/) /
  [app-editors/neovim](https://packages.gentoo.org/packages/app-editors/neovim),
  text editor)
	* par
	  ([aur/par](https://aur.archlinux.org/packages/par/) /
	  [app-text/par](https://packages.gentoo.org/packages/app-text/par)
	  for nice text reflowing)
	* xclip
	  ([extra/xclip](https://www.archlinux.org/packages/extra/x86_64/xclip/) /
	  [x11-misc/xclip](https://packages.gentoo.org/packages/x11-misc/xclip)
	  for clipboard integration on X)
	* [YouCompleteMe](https://github.com/ycm-core/YouCompleteMe)'s dependencies
	* ripgrep
	  ([community/ripgrep](https://www.archlinux.org/packages/community/x86_64/ripgrep/) /
	  [sys-apps/ripgrep](https://packages.gentoo.org/packages/sys-apps/ripgrep),
	  smart grep)
* acpilight
  ([community/acpilight](https://archlinux.org/packages/community/any/acpilight/) /
  [sys-power/acpilight](https://packages.gentoo.org/packages/sys-power/acpilight),
  an xbacklight replacement that directly uses the kernel's API, also works without X)
* driver for your video card that supports GLX

## Installation

You are encouraged to copy stuff you like to your own configuration files. If you want to copy my setup, you can use the script `install.sh` (that script is normally mostly for myself though).

Create a symlink to a background image in `~/.config/i3/background` or `~/.config/sway/background`, and to a lockscreen image in `~/.config/i3/background_lock` or `~/.config/sway/background_lock`.
