#!/bin/false

function my_pwd {
	# ~ for home; keep only the last two directories
	echo "$PWD" | sed -r '
		s,^'"$HOME"',~,; t reset; :reset
		s|^~/git/my-cool-project/([^/])[^/]+/([^/]+)/|\1/\2/|; t end
		s|^.+/([^/]*/[^/]*)$|\1|
		:end'
}

function my_retval {
	RETVAL=$?

	if [[ $RETVAL -eq 0 ]]; then
		echo "%{%F{yellow}%}"
	else
		echo "%{%F{red}%}$RETVAL "
	fi
}

function precmd {
	PS1="$(my_retval)$(my_pwd)$(git_prompt_info)$(git_prompt_status) %{%f%} "
}

ZSH_THEME_GIT_PROMPT_PREFIX=" %{$fg[green]%}  "
ZSH_THEME_GIT_PROMPT_SUFFIX=""
ZSH_THEME_GIT_PROMPT_DIRTY=""
ZSH_THEME_GIT_PROMPT_CLEAN=""
ZSH_THEME_GIT_PROMPT_ADDED=" %{$fg[cyan]%}+"
ZSH_THEME_GIT_PROMPT_MODIFIED=" %{$fg[yellow]%}✭"
ZSH_THEME_GIT_PROMPT_DELETED=" %{$fg[red]%}✗"
ZSH_THEME_GIT_PROMPT_RENAMED=" %{$fg[blue]%}➦"
ZSH_THEME_GIT_PROMPT_UNMERGED=" %{$fg[magenta]%}✂"
ZSH_THEME_GIT_PROMPT_UNTRACKED=" %{$fg[grey]%}✱"

ZSH_THEME_TERM_TAB_TITLE_IDLE="in %~" # NOT truncated PWD


#  vim: set ft=zsh noet :
