#!/bin/bash

GUI=true
[[ $1 == --no-x ]] && GUI=false

DOT_CONF="${XDG_CONFIG_HOME:-$HOME/.config}"
DOT_LOCAL_BIN="$HOME/.local/bin"
mkdir -p "$DOT_CONF"

# stuff that we only want when we're setting up a graphical environment
[[ $GUI == true ]] && {
	ln -s $PWD/xinitrc        $HOME/.xinitrc
	ln -s $PWD/Xresources     $HOME/.Xresources

	mkdir -p "$DOT_LOCAL_BIN"

	mkdir -p $DOT_CONF/{dunst,compton,xfce4,foot}
	ln -s $PWD/i3/            $DOT_CONF
	ln -s $PWD/sway/          $DOT_CONF
	ln -s $PWD/dunstrc.ini    $DOT_CONF/dunst/dunstrc
	ln -s $PWD/compton.conf   $DOT_CONF/compton/config
	ln -s $PWD/rofi/          $DOT_CONF
	ln -s $PWD/polybar/       $DOT_CONF
	ln -s $PWD/yambar/        $DOT_CONF
	ln -s $PWD/redshift.ini   $DOT_CONF/redshift.conf
	ln -s $PWD/xfce4-terminal $DOT_CONF/xfce4/terminal
	ln -s $PWD/foot.ini       $DOT_CONF/foot/foot.ini
	ln -s -t $DOT_LOCAL_BIN     $PWD/bin/*

	sudo mkdir -p /usr/share/fonts/local
	sudo ln -s $PWD/yambar/bar-material-supplements.ttf /usr/share/fonts/local/
}

mkdir -p $DOT_CONF/{nvim/plug,beets}
ln -s $PWD/profile           $HOME/.profile
ln -s $PWD/gitignore         $HOME/.gitignore
ln -s $PWD/nvim-init.vim     $DOT_CONF/nvim/init.vim
ln -s $PWD/user-dirs.dirs    $DOT_CONF/user-dirs.dirs
ln -s $PWD/beets.config.yaml $DOT_CONF/beets/config.yaml

nvim -c :PlugUpgrade -c :PlugInstall -c :PlugUpdate
