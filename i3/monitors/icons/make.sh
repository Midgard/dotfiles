#!/bin/bash

laptop=edp-1
ext1=hdmi-1; ext1_three=external-hdmi
ext2=vga-1; ext2_three=external-vga

tmp=/tmp/screens-icons-generator.svg

rm -f *actv*.png

black() {
	cp "$1" "$tmp"
	shift
	for i in "$@"; do
		sed -i 's/\(id="'"$i"'" style="fill:#\)ffffff;/\1000000;/' "$tmp"
	done
}

# One screen
inkscape -e "actv-${laptop}.png" ./src/one-screen.svg

# Two screens
inkscape -e "actv-${laptop}_actv-${ext1}.png" ./src/two-screens.svg
cp "actv-${laptop}_actv-${ext1}.png" "actv-${laptop}_actv-${ext2}.png"

black ./src/two-screens.svg laptop
inkscape -e "non-${laptop}_actv-${ext1}.png" "$tmp"
cp "non-${laptop}_actv-${ext1}.png" "non-${laptop}_actv-${ext2}.png"

black ./src/two-screens.svg external
inkscape -e "actv-${laptop}_non-${ext1}.png" "$tmp"
cp "actv-${laptop}_non-${ext1}.png" "actv-${laptop}_non-${ext2}.png"

# Three screens
inkscape -e "actv-${laptop}_actv-${ext1}_actv-${ext2}.png" ./src/three-screens.svg

black ./src/three-screens.svg laptop
inkscape -e "non-${laptop}_actv-${ext1}_actv-${ext2}.png" "$tmp"

black ./src/three-screens.svg "$ext1_three"
inkscape -e "actv-${laptop}_non-${ext1}_actv-${ext2}.png" "$tmp"

black ./src/three-screens.svg "$ext2_three"
inkscape -e "actv-${laptop}_actv-${ext1}_non-${ext2}.png" "$tmp"

black ./src/three-screens.svg "$ext1_three" "$ext2_three"
inkscape -e "actv-${laptop}_non-${ext1}_non-${ext2}.png" "$tmp"

black ./src/three-screens.svg laptop "$ext2_three"
inkscape -e "non-${laptop}_actv-${ext1}_non-${ext2}.png" "$tmp"

black ./src/three-screens.svg laptop "$ext1_three"
inkscape -e "non-${laptop}_non-${ext1}_actv-${ext2}.png" "$tmp"
