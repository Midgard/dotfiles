#!/bin/bash

set -euo pipefail

# Control screen brightness in a logarithmic fashion. Linear backlight control
# just annoys me: I want fine-grained control over low brightnesses and quickly
# change the value at high brightnesses.
#
# We assume the minimum brightness is 0 (light off) and the maximum brightness
# is 100. This is the behaviour of
# [acpilight](https://github.com/wavexx/acpilight/), a replacement for
# xorg-xbacklight (which doesn't work on my laptop for some reason).
#
# Use `backlight.sh up` or `backlight.sh down` to increase or decrease the
# backlight brightess. If you want to adjust the speed with which it changes,
# adjust `change_percent` below.


# Multiply the current brightness by this percentage. This has to be an integer
change_percent=150

# Don't start a new transition if the previous one isn't done yet
pgrep -u $UID -x xbacklight >/dev/null && exit

# Calculate new target brightness
current_brightness=$(xbacklight -get)

new_target() {
	echo $current_brightness >&2
	case $1 in
		up)
			if [ $current_brightness = 0 ]; then
				echo 1
				return
			fi

			target=$((current_brightness * $change_percent / 100))
			[ $target -ne $current_brightness ] || target=$((target + 1))
			;;

		down)
			if [ $current_brightness = 0 ]; then
				echo 0.1
				return
			fi

			target=$((current_brightness * 100 / $change_percent))
			[ $target -ne $current_brightness ] || target=$((target - 1))
			;;
	esac

	# Boundaries
	if [ $target -le 1   ]; then echo 0.1; return; fi
	if [ $target -ge 100 ]; then echo 100; return; fi

	echo $target
}

target=$(new_target "$1")

echo $target
# Smoothly set the new brightness
xbacklight -time 100 -fps 60 -set $target
