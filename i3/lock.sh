#!/bin/bash

set -euo pipefail

#ssh_agent_file="/run/user/$EUID/ssh-agent-session"
#if pgrep -u "$USER" ssh-agent > /dev/null && [[ -f "$ssh_agent_file" ]]; then
#	source "$ssh_agent_file"
#	if ! ssh-add -D; then
#		notify-send -u normal -i lock -a i3 'Failed to clear SSH key cache' 'ssh-add had a non-zero exit code.'
#	fi
#else
#	notify-send -u normal -i lock -a i3 'No SSH agent running' 'No running SSH agent detected, so not clearing the key cache.'
#fi

if ! gpg-connect-agent reloadagent /bye; then
	:
#	notify-send -u normal -i lock -a i3 'Failed to clear GPG key cache' 'gpg-connect-agent had a non-zero exit code.'
fi

if ! i3lock --tiling --lock-console --show-failed-attempts --color=000000 --image="$HOME/.config/i3/background_lock"; then
	exec i3lock
fi
