#!/bin/bash

scriptdir="$(dirname "$0")"

source "$scriptdir/monitors/config"
[[ -n $builtin ]] || { echo "builtin empty"; exit 1; }
[[ -n $ext1 ]]    || { echo "ext1 empty";    exit 1; }
[[ -n $ext2 ]]    || { echo "ext2 empty";    exit 1; }

xq="$(xrandr --query)"
xlm="$(xrandr --listmonitors)"

card() { echo "$xq"  | grep -qF "$1";           }
conn() { echo "$xq"  | grep -qF "$1 connected"; }
actv() { echo "$xlm" | grep -qF "$1";           }

should_exist() {
	for i in "$@"; do
		card "$i" || { echo "$i not on video card, refusing to do anything"; exit 1; }
	done
}
should_exist "$builtin" "$ext1" "$ext2"

trigger_change_and_exit() {
	"$scriptdir/../polybar/launch.sh" & disown
	"$scriptdir/background.sh"
	exit
}

builtin_off() {
	level=$(xbacklight -get)
	if [[ $level -gt 0 ]]; then
		echo $level >/tmp/.brightness
		xbacklight -time 500 -fps 30 -set 0
	fi
}
builtin_on() {
	level=$(xbacklight -get)
	if [[ $level -eq 0 ]]; then
		wanted=$([[ -r /tmp/.brightness ]] && cat /tmp/.brightness || echo "50")
	elif [[ $level -lt 10 ]]; then
		wanted=10
	fi
	xbacklight -time 500 -fps 30 -set $wanted
}

single_screen() {
	one="$1"
	off1="$2"
	off2="$3"

	xrandr \
		--output "$one" --auto --primary \
		--output "$off1" --off \
		--output "$off2" --off
	builtin_on
	notify-send -a 'Monitor layout toggler' -i display "Only $one connected" "Disabled all outputs except for $one."
	trigger_change_and_exit
}

toggle_two_screens() {
	one="$1"
	two="$2"
	off="$3"

	if actv "$one" && actv "$two"; then
		xrandr \
			--output "$one" --off \
			--output "$two" --auto --primary \
			--output "$off" --off
		builtin_off
	elif ! actv "$one" && actv "$two"; then
		xrandr \
			--output "$one" --auto --primary \
			--output "$two" --off \
			--output "$off" --off
		builtin_on
	else
		xrandr \
			--output "$one" --auto --primary \
			--output "$two" --auto --right-of "$builtin" \
			--output "$off" --off
		builtin_on
	fi
	trigger_change_and_exit
}

toggle_three_screens() {
	one="$1"
	two="$2"
	three="$3"

	if actv "$one" && actv "$two" && actv "$three"; then
		xrandr \
			--output "$one" --off \
			--output "$two" --auto --primary \
			--output "$three" --auto --right-of "$two"
		builtin_off
	elif ! actv "$one" && actv "$two" && actv "$three"; then
		xrandr \
			--output "$one" --auto --primary \
			--output "$two" --off \
			--output "$three" --auto --right-of "$one"
		builtin_on
	elif actv "$one" && ! actv "$two" && actv "$three"; then
		xrandr \
			--output "$one" --auto --primary \
			--output "$two" --auto --right-of "$one" \
			--output "$three" --off
		builtin_on
	elif actv "$one" && actv "$two" && ! actv "$three"; then
		xrandr \
			--output "$one" --auto --primary \
			--output "$two" --off \
			--output "$three" --off
		builtin_on
	elif actv "$one" && ! actv "$two" && ! actv "$three"; then
		xrandr \
			--output "$one" --off \
			--output "$two" --auto --primary \
			--output "$three" --off
		builtin_off
	elif ! actv "$one" && actv "$two" && ! actv "$three"; then
		xrandr \
			--output "$one" --off \
			--output "$two" --off \
			--output "$three" --auto --primary
		builtin_off
	else
		xrandr \
			--output "$one" --auto --primary \
			--output "$two" --auto --right-of "$one" \
			--output "$three" --auto --right-of "$two"
		builtin_on
	fi
	trigger_change_and_exit
}

# Only builtin connected
if conn "$builtin" && ! conn "$ext1" && ! conn "$ext2"; then
	single_screen "$builtin" "$ext1" "$ext2"
fi

# Only builtin and ext1 connected
if conn "$builtin" && conn "$ext1" && ! conn "$ext2"; then
	toggle_two_screens "$builtin" "$ext1" "$ext2"
fi

# Only builtin and ext2 connected
if conn "$builtin" && ! conn "$ext1" && conn "$ext2"; then
	toggle_two_screens "$builtin" "$ext2" "$ext1"
fi

# Builtin, ext1 and ext2 connected
if conn "$builtin" && conn "$ext1" && conn "$ext2"; then
	toggle_three_screens "$builtin" "$ext1" "$ext2"
fi

# Other cases: something's wrong, try enabling all displays
xrandr \
	--output "$builtin" --auto --primary \
	--output "$ext1" --auto \
	--output "$ext2" --auto
builtin_on
