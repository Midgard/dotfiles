#!/bin/bash

background="$HOME/.config/i3/background"
[[ -n $1 ]] && background="$1"

exec feh --bg-fill "$background"
