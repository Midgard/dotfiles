#!/bin/bash

# If a window with a path in its title is focused, open a terminal with that
# location as working directory. Like this, terminals are opened in the same
# directory as the currently focused one.
#
# Your shell should set the window title correctly, of course. In most common
# terminals this can be achieved with
# printf '\033]2;%s@%s:%s\007' "${USER}" "${HOSTNAME%%.*}" "${PWD}"
# In bash, you can put this printf in your PROMPT_COMMAND.

set -uo pipefail
# A failure in this script should not prevent the terminal from opening
set +e

#CMD=i3-sensible-terminal
if [ -n "${WAYLAND_DISPLAY:-}" ]; then
	CMD=foot
else
	CMD=xfce4-terminal
fi

focused_window_title() {
	if [ -n "${WAYLAND_DISPLAY:-}" ]; then
		lswt -j | jq -r '.toplevels | map(select(.activated?))[0].title'
	else
		xtitle
	fi
}

title="$(focused_window_title)"
path=""

if [[ $title =~ (in|^.*@$(hostname):)(~?)(/.*) ]]; then
	path="${BASH_REMATCH[3]}"
	[[ ${BASH_REMATCH[2]} == "~" ]] && path="$HOME$path"

	while [[ $path != / && -n $path && ! -d $path ]]; do
		path="$(dirname "$path")"
	done
fi

# Open terminal at specified path
if [[ -n $path ]] && [[ -r $path ]]; then
	exec env -C "$path" $CMD "$@"
else
	exec $CMD "$@"
fi
