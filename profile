export PATH="$PATH:$HOME/.local/bin:$HOME/.cargo/bin"

export EDITOR=vim
export VISUAL="$EDITOR"

export TERMINAL=xfce4-terminal

export PARINIT="rTbgqR B=.,?_A_a Q=_s>|"

export PIPENV_VENV_IN_PROJECT=true

export LEDGER_FILE="$HOME/git/ledger/$(date +%Y).journal"

# Automatically start x when logging in on tty1
if [ ! "$DISPLAY" -a "$XDG_VTNR" -eq 1 ]; then
	exec startx
fi
