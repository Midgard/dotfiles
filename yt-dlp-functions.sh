ytd() { # yt download
	yt-dlp \
		--merge-output-format mkv --embed-chapters \
		--all-subs --embed-subs \
		--format="((248/bestvideo[height<=1080])+(251/bestaudio))/best" \
		"$@"
}
ytdb() { # yt download best
	yt-dlp \
		--merge-output-format mkv --embed-chapters \
		--all-subs --embed-subs \
		--format="(bestvideo+bestaudio)/best" \
		"$@"
}
ytda() { # yt download audio
	yt-dlp --extract-audio --format="bestaudio" "$@"
}

ytdfpl() { # yt download fixed playlist
	# Same order as on site, we use this for playlists that will not grow any more ("fixed")
	ytd \
		--download-archive downloaded.list \
		-o "%(playlist_index)04d-%(title)s-%(id)s.%(ext)s" \
		"$@"
}
ytdpl() { # yt download playlist
	# --playlist-reverse: Reversed order, we use this for playlists that will grow
	ytdfpl --playlist-reverse "$@"
}

ytdafpl() { # yt download audio fixed playlist
	# Same order as on site, we use this for playlists that will not grow any more ("fixed")
	ytda \
		--download-archive downloaded.list \
		-o "%(playlist_index)04d-%(title)s-%(id)s.%(ext)s" \
		"$@"
}
ytdapl() { # yt download audio playlist
	# --playlist-reverse: Reversed order, we use this for playlists that will grow
	ytdafpl --playlist-reverse "$@"
}
