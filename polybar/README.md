# Polybar configuration

![](../.repo-stuff/bar.png)

System monitors for CPU usage, RAM usage and temperature are shown when their values are high. When the CPU gets a moderate load it looks like this:

![](../.repo-stuff/bar-load.png)

To be able to put my config in Git and keep some stuff I don't want to share confidential (my MPD server host and password), I created a `secrets.sh` file. Mine looks like this:

```sh
#!/bin/false

MPD_ONLINE_1_HOST=mpd.example.org
MPD_ONLINE_1_PORT=6600
MPD_ONLINE_1_PASS=mypass1

MPD_ONLINE_2_HOST=mpd2.example.org
MPD_ONLINE_2_PORT=1234
MPD_ONLINE_2_PASS=mypass2
```
