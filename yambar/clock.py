#!/usr/bin/env python3

import sys
from datetime import datetime
import time
import pytz


# if len(sys.argv) < 3:
    # print("Usage: {scriptname} timezone_left timezone_right".format(scriptname=sys.argv[0]), file=sys.stderr)
    # exit(1)

# try:
    # tzleft  = pytz.timezone(sys.argv[1])
    # tzright = pytz.timezone(sys.argv[2])
# except pytz.exceptions.UnknownTimeZoneError as e:
    # print('Unknown time zone "{}"'.format(e.args[0]), file=sys.stderr)
    # exit(1)


hourhands   = ""
minutehands = ""

def hands_for(t):
    minute = minutehands[ t.minute // 5 ]
    hour   = hourhands  [ t.hour % 12   ]
    return hour + minute


def time_of_day(t):
    if t.hour < 5 or t.hour > 22:
        return "night"
    elif t.hour < 10:
        return "morning"
    elif t.hour < 13:
        return "noon"
    elif t.hour < 19:
        return "afternoon"
    else:
        return "evening"


while True:
    utcnow   = datetime.now(tz=pytz.utc)
    localnow = datetime.now()
    # leftnow  = utcnow.astimezone(tzleft )
    # rightnow = utcnow.astimezone(tzright)

    print(f"""time_of_day|string|{time_of_day(localnow)}
hands|string|{hands_for(localnow)}
hour|string|{localnow.strftime("%H")}
minute|string|{localnow.strftime("%M")}
weekday|string|{localnow.strftime("%A")}
date|string|{localnow.strftime("%d %b")}
""", flush = True)

    delay = 60 - localnow.second - (localnow.microsecond * 10e-7)
    time.sleep(delay)
