#!/bin/sh

temp="$( (cat /sys/class/thermal/thermal_zone*/temp 2>/dev/null | maths max | tr -d $'\n'; echo " / 1000") | bc)"
rpm="$(sudo fan_control rpm 1)"
if test "$temp" -ge 60 && test "${rpm% RPM}" -lt 100; then
	rpm="$(sudo fan_control rpm 1)"
fi

echo "temp|int|$temp"
echo "rpm|int|${rpm% RPM}"
if test "$temp" -ge 90; then
	status=alert
elif test "$temp" -ge 80; then
	status=caution
elif test "$temp" -ge 60 || test "${rpm% RPM}" -gt 3000; then
	status=active
else
	status=normal
fi
echo "status|string|$status"
echo
